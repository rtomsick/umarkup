package org.umarkup.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.umarkup.model.Element;
import org.umarkup.model.Element.CloseType;
import org.umarkup.render.SimpleRenderer;
import org.umarkup.model.MuElement;

/**
 * Contains tests for various edge cases, strange things, and otherwise 
 * unconventional behaviors.
 * 
 * This includes tests for things that look suspiciously like XML/SGML elements,
 * yet really aren't (like DOCTYPE decs.)
 */
public final class StrangeThingsTest
{

    @DisplayName("DOCTYPE-style elements")
    @Test
    public final void docTypeStyleElements()
    {
        final Element<?> dtElem = 
            MuElement.from("!DOCTYPE html").close(CloseType.NEVER);

        final String rendered = 
            (new SimpleRenderer()).render(dtElem).toString().trim();

        Assertions.assertEquals("<!DOCTYPE html>", rendered);
        
    }
}