package org.umarkup.test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.umarkup.model.MuElement;
import org.umarkup.model.Element.CloseType;
import org.umarkup.render.Renderer;
import org.umarkup.render.SimpleRenderer;
import org.xml.sax.InputSource;

import nu.validator.messages.MessageEmitter;
import nu.validator.messages.MessageEmitterAdapter;
import nu.validator.messages.TextMessageEmitter;
import nu.validator.servlet.imagereview.ImageCollector;
import nu.validator.source.SourceCode;
import nu.validator.validation.SimpleDocumentValidator;
import nu.validator.xml.SystemErrErrorHandler;

public final class SimpleValidationTest
{

    public static final String      TEST_STRING = "This is a test.";

    @DisplayName("Content smoke test")
    @Test
    public void simpleContentCheck()
    {
        MuElement test = new MuElement("html");

        test.children(MuElement.from("body")
                        .children(MuElement.from("p")
                                    .child(TEST_STRING)));

        final CharSequence rendered = (new SimpleRenderer()).render(test);

        Assertions.assertTrue(rendered.toString().contains(TEST_STRING));
    }

    @Test
    public void validationTest()
    {
        final MuElement dt = new MuElement("!DOCTYPE html").close(CloseType.NEVER);

        MuElement test = new MuElement("html");

        test = test.children(MuElement.from("head")
                            .children(MuElement.from("title").child("test")),
                        MuElement.from("body")
                            .children(MuElement.from("p")
                                        .child(TEST_STRING)));

        Renderer renderer = new SimpleRenderer();

        final CharSequence renderedRoot = renderer.render(test);

        final String html = renderer.render(dt).toString() + 
                            renderedRoot.toString();

        try 
        {
            int failures = validateHtml(html);
            Assertions.assertEquals(0, failures);
        } 
        catch (Exception e)
        {
			Assertions.fail(e);
		}
    }

    private static final int validateHtml(String htmlContent)
    throws Exception 
    {

        InputStream in = new ByteArrayInputStream(htmlContent.getBytes( "UTF-8" ));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
    
        SourceCode sourceCode = new SourceCode();
        ImageCollector imageCollector = new ImageCollector(sourceCode);
        boolean showSource = false;
        MessageEmitter emitter = new TextMessageEmitter( out, false );
        MessageEmitterAdapter errorHandler = new MessageEmitterAdapter( sourceCode, showSource, imageCollector, 0, false, emitter );
        errorHandler.setErrorsOnly( true );
    
        SimpleDocumentValidator validator = new SimpleDocumentValidator();
        validator.setUpMainSchema( "http://s.validator.nu/html5-rdfalite.rnc", new SystemErrErrorHandler());
        validator.setUpValidatorAndParsers( errorHandler, true, false );
        validator.checkHtmlInputSource( new InputSource(in));
    
        

        return errorHandler.getErrors();
    }
}