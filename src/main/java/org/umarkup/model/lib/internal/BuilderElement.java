package org.umarkup.model.lib.internal;

import java.util.Collection;
import java.util.List;

import org.umarkup.model.Attribute;
import org.umarkup.model.Element;
import org.umarkup.model.MuElement;
import org.umarkup.model.MutableElement;

/**
 * <p>
 * Builder element.  Behaves exactly like a {@link MuElement}, however all 
 * methods specified by {@link MutableElement} return a mutable copy of the
 * element, leaving the object on which they are invoked unchanged.
 * </p>
 * 
 * <p>
 * Intended to support library implementations within the 
 * {@code org.umarkup.lib} package.
 * </p>
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public class BuilderElement
extends MuElement
{

	public BuilderElement(String name, CloseType closingPolicy)
	{
		super(name);
		super.close(closingPolicy);
	}
	
	public BuilderElement(String name)
	{
		super(name);
	}
	
	private BuilderElement(String name, List<Element<?>> children, 
							Collection<Attribute> attributes)
	{
		this(name);
		this.children(children);
		this.attributes(attributes);
	}

	@Override
	public MuElement 
	name(String name)
	{
		return ((MuElement)super.clone()).name(name);
	}

	@Override
	public MuElement 
	attribute(Attribute attribute)
	{
		return ((MuElement)super.clone()).attribute(attribute);
	}
	
	@Override
	public MuElement 
	attribute(String name, String value)
	{
		return ((MuElement)super.clone()).attribute(name, value);
	}

	@Override
	public MuElement 
	attributes(Collection<Attribute> attributes)
	{
		return ((MuElement)super.clone()).attributes(attributes);
	}

	@Override
	public MuElement 
	attribute(String name, char delimiter, String... values)
	{
		return ((MuElement)super.clone()).attribute(name, delimiter, values);
	}

	@Override
	public MuElement 
	attributes(Attribute... attributes)
	{
		return ((MuElement)super.clone()).attributes(attributes);
	}

	@Override
	public MuElement 
	child(Element<?> child)
	{
		return ((MuElement)super.clone()).child(child);
	}

	@Override
	public MuElement 
	child(String child)
	{
		return ((MuElement)super.clone()).child(child);
	}

	@Override
	public MuElement 
	children(List<Element<?>> children)
	{
		return ((MuElement)super.clone()).children(children);
	}
	
	@Override
	public MuElement 
	children(Element<?>... children)
	{
		return ((MuElement)super.clone()).children(children);
	}

	@Override
	public MuElement 
	close(CloseType policy)
	{
		return ((MuElement)super.clone()).close(policy);
	}

	@Override
	public Object 
	clone()
	{
		return new BuilderElement(this.name(), this.children(), 
									this.attributes());
	}

}
