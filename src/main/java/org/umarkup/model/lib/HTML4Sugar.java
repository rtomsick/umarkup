package org.umarkup.model.lib;

import org.umarkup.model.Element;
import org.umarkup.model.MuElement;

import static org.umarkup.model.lib.HTML4.Header.*;
import static org.umarkup.model.lib.HTML4.Form.*;
import static org.umarkup.model.lib.HTML4.Document.*;
import static org.umarkup.model.lib.HTML4.Body.*;

public abstract class HTML4Sugar
extends HTML4
{
	public static final MuElement
	H1(String content)
	{
		return H1.child(content);
	}
	
	public static final MuElement
	H2(String content)
	{
		return H2.child(content);
	}
	
	public static final MuElement
	H3(String content)
	{
		return H3.child(content);
	}
	
	public static final MuElement
	H4(String content)
	{
		return H4.child(content);
	}
	
	public static final MuElement
	H5(String content)
	{
		return H5.child(content);
	}
	
	public static final MuElement
	H6(String content)
	{
		return H6.child(content);
	}
	
	
	
	public static final MuElement
	DIV(Element<?> ... elements)
	{
		return DIV.children(elements);
	}
	
	public static final MuElement
	A(String ... contents)
	{
		return _mkElem(A, contents);
	}
	
	public static final MuElement
	P(Element<?> ... elements)
	{
		return P.children(elements);
	}
	
	public static final MuElement
	P(String ... children)
	{
		return _mkElem(P, children);
	}
	
	public static final MuElement
	B(String ... children)
	{
		return _mkElem(B, children);
	}
	
	public static final MuElement
	SPAN(String ... children)
	{
		return _mkElem(SPAN, children);
	}
	
	public static final MuElement
	IMG(String srcURL)
	{
		return IMG.attribute("src", srcURL);
	}
	
	public static final MuElement
	IMG(String srcURL, String altText)
	{
		return IMG.attribute("src", srcURL)
					.attribute("alt", altText);
	}
	
	
	public static final MuElement 
	LEGEND(String content)
	{
		return LEGEND.child(content);
	}
	
	
	
	public static final MuElement
	BODY(Element<?> ... elements)
	{
		return BODY.children(elements);
	}
	
	public static final MuElement
	TITLE(String ... children)
	{
		return _mkElem(TITLE, children);
	}
	
	public static final MuElement
	HEAD(Element<?> ... elements)
	{
		return HEAD.children(elements);
	}
	
	public static final MuElement
	HTML(Element<?> ... elements)
	{
		return HTML.children(elements);
	}
	
	
	private static final MuElement
	_mkElem(MuElement mu, String ... values)
	{
		MuElement m = mu;
		for (String e : values)
		{
			m = m.child(e);
		}
		return m;
	}

}
