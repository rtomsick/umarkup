package org.umarkup.model.lib;

import org.umarkup.model.MuElement;
import org.umarkup.model.lib.internal.BuilderElement;

/**
 * HTML 4 library; provides static builder objects for most (but not all) 
 * elements defined by the HTML 4.01 specification.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 * @version 0.2
 */
public abstract class HTML4
{

	/**
	 * Heading elements (H1 through H6).
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class Header
	{
		public static final MuElement H1 = 
				new BuilderElement("h1");
		
		public static final MuElement H2 = 
				new BuilderElement("h2");
		
		public static final MuElement H3 =
				new BuilderElement("h3");
		
		public static final MuElement H4 =
				new BuilderElement("h4");

		public static final MuElement H5 = 
				new BuilderElement("h5");
		
		public static final MuElement H6 =
				new BuilderElement("h6");
	}

	/**
	 * Elements typically found within body text of an HTML 4 document (bold,
	 * italics, spans, etc.)
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class Body
	{
		public static final MuElement DIV =
				new BuilderElement("div");
		
		public static final MuElement P =
				new BuilderElement("p");
		
		public static final MuElement SPAN = 
				new BuilderElement("span");
		
		public static final MuElement A = 
				new BuilderElement("a");
		
		public static final MuElement B = 
				new BuilderElement("b");
		
		public static final MuElement I = 
				new BuilderElement("i");
		
		public static final MuElement U = 
				new BuilderElement("u");
		
		public static final MuElement STRONG =
				new BuilderElement("strong");
		
		public static final MuElement EM = 
				new BuilderElement("em");
		
		public static final MuElement IMG =
				new BuilderElement("img");
	}
	
	/**
	 * Table elements.
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class Table
	{
		public static final MuElement TABLE =
				new BuilderElement("table");
		
		public static final MuElement TH =
				new BuilderElement("th");
		
		public static final MuElement TD = 
				new BuilderElement("td");
		
		public static final MuElement TR = 
				new BuilderElement("tr");
	}
	
	/**
	 * Ordered and unordered lists and list items.
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class List
	{
		public static final MuElement UL = 
				new BuilderElement("ul");
		
		public static final MuElement OL = 
				new BuilderElement("ol");
		
		public static final MuElement LI =
				new BuilderElement("li");
	}
	
	/**
	 * Form-related elements (forms, fieldsets, inputs, etc.)
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class Form
	{
		public static final MuElement FORM = 
				new BuilderElement("form");
		
		public static final MuElement FIELDSET = 
				new BuilderElement("fieldset");
		
		public static final MuElement LEGEND = 
				new BuilderElement("legend");
		
		public static final MuElement INPUT = 
				new BuilderElement("input");
		
		public static final MuElement TEXTAREA = 
				new BuilderElement("textarea");
				
		public static final MuElement LABEL = 
				new BuilderElement("label");
	}
	
	/**
	 * Assorted document elements (head, title, body, script, etc.)
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public static abstract class Document
	{
		public static final MuElement BODY = 
				new BuilderElement("body");
		
		public static final MuElement TITLE =
				new BuilderElement("title");
		
		public static final MuElement META =
				new BuilderElement("meta");
		
		public static final MuElement HEAD = 
				new BuilderElement("head");
		
		public static final MuElement STYLE =
				new BuilderElement("style");
		
		public static final MuElement SCRIPT = 
				new BuilderElement("script");
		
		public static final MuElement HTML = 
				new BuilderElement("html");
	}
		
}
