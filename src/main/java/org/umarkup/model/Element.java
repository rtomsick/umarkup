package org.umarkup.model;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Defines a markup element.  An element has a name (ex: in HTML, "h1" for a 
 * heading element) and optionally may have 1+ attributes.  It may also have
 * 1+ child elements which themselves may be {@link Element}s or arbitrary
 * {@link CharSequence}s.
 * </p>
 * 
 * <p>
 * All markup elements may be rendered to {@link CharSequence} objects or to
 * {@link String}s.  During rendering, each child element will be rendered to
 * {@link CharSequence} form in the order in which it has been added to its
 * parent element.
 * </p>
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 * @param <E> element type
 */
public interface Element<E extends Element<?>>
{

	/**
	 * Get the name of the element.
	 * 
	 * @return element name
	 */
	public String name();
	
	/**
	 * <p>
	 * Get the attributes of the element.
	 * </p>
	 * 
	 * <p>
	 * The resulting collection will reflect the attributes present on the 
	 * element at the time of invocation but will not be modifiable.
	 * </p>
	 * 
	 * @return element attributes
	 */
	public Collection<Attribute> attributes();
	
	/**
	 * <p>
	 * Get the children of the element.
	 * </p>
	 * 
	 * <p>
	 * The resulting list will reflect the children of the element at the time
	 * of invocation but will not be modifiable.
	 * </p>
	 * 
	 * @return children
	 */
	public List<Element<?>> children();
	
	/**
	 * Create a template from the element.
	 * 
	 * @return template based on the element
	 */
	public Template<E> template();
	
	/**
	 * Get the closing policy of the element.
	 * 
	 * @return element closing policy
	 */
	public CloseType closing();

	
	/**
	 * Element closing policies.
	 * 
	 * @author Robert Tomsick (robert@tomsick.net)
	 *
	 */
	public enum CloseType
	{
		/**
		 * Elements self-close when no children are present, explicitly 
		 * close otherwise.
		 */
		SELF,
		/**
		 * Elements explicitly close (i.e. close tag always present).
		 */
		EXPLICIT,
		/**
		 * Elements explicitly close only when children are present, unclosed
		 * otherwise.
		 */
		CHILDREN,
		/**
		 * Elements are never closed.
		 */
		NEVER;
	}
	
}
