package org.umarkup.model;

/**
 * An anonymous element has no name, no children, and no attributes.  It 
 * consists only of character content.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public interface AnonymousElement
extends Element<AnonymousElement>
{

	/**
	 * Get the string content of the element.
	 * 
	 * @return string content
	 */
	public abstract CharSequence content();

	/**
	 * Set the string content of the element.
	 * 
	 * @param content content
	 * @return element with the given content set
	 */
	public abstract AnonymousElement content(CharSequence content);
}