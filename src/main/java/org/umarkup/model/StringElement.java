package org.umarkup.model;

import java.util.Collections;
import java.util.List;

/**
 * <p>
 * A string elements consists solely of a given string.  It contains no
 * children and has no attributes.
 * </p>
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public class StringElement
implements AnonymousElement
{

	private CharSequence content;

	/**
	 * Construct a new element from the given content.
	 * 
	 * @param content element content
	 */
	public StringElement(CharSequence content)
	{
		this.content = content;
	}
	
	/* (non-Javadoc)
	 * @see org.umarkup.model.AnonymousElement#content()
	 */
	@Override
	public CharSequence
	content()
	{
		return this.content;
	}
	
	/* (non-Javadoc)
	 * @see org.umarkup.model.AnonymousElement#content(java.lang.String)
	 */
	@Override
	public AnonymousElement
	content(CharSequence content)
	{
		this.content = content;
		return this;
	}
	
	@Override
	public String 
	name()
	{
		return "";
	}

	@Override
	public List<Attribute> 
	attributes()
	{
		return Collections.emptyList();
	}

	@Override
	public List<Element<?>> 
	children()
	{
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see org.umarkup.model.AnonymousElement#template()
	 */
	@Override
	public Template<AnonymousElement>
	template()
	{
		return new StringTemplate(this);
	}

	@Override
	public CloseType 
	closing()
	{
		return CloseType.EXPLICIT;
	}
	
	/**
	 * Convenience method to create a new element from the given content 
	 * string.  The content will be included in the resulting element without
	 * escaping/translation.
	 * 
	 * @param content element content
	 * @return new element with the given content
	 */
	public static final AnonymousElement 
	str(String content)
	{
		return new StringElement(content);
	}
	
	/**
	 * Convenience method to creat a new element from the given content
	 * string.  The content will have HTML entities escaped prior to use in
	 * the newly-constructed element.
	 * 
	 * @param content element content
	 * @return new element with the given content (with HTML entities escaped)
	 */
	public static final AnonymousElement
	_str(String content)
	{
		return str(content.replace("&", "&amp;")
							.replace("<", "&lt;")
							.replace(">", "&gt;"));
	}

	/**
	 * Template for {@link StringElement}s.  Stores only the content of the 
	 * element (since elements produced by the template can have neither 
	 * children nor attributes.)
	 * 
	 */
	protected static class StringTemplate
	implements Template<AnonymousElement>
	{
		private final CharSequence content;

		protected StringTemplate(String content)
		{
			this.content = content;
		}
		
		protected StringTemplate(AnonymousElement element)
		{
			this.content = element.content();
		}
		
		@Override
		public String 
		name()
		{
			return "";
		}

		@Override
		public List<Element<?>> 
		children()
		{
			return Collections.emptyList();
		}

		@Override
		public List<Attribute> 
		attributes()
		{
			return Collections.emptyList();
		}

		@Override
		public AnonymousElement 
		element()
		{
			return new StringElement(this.content);
		}

		@Override
		public Template<AnonymousElement> 
		template()
		{
			return this;
		}

		@Override
		public CloseType 
		closing()
		{
			return CloseType.EXPLICIT;
		}
		
	}
}
