package org.umarkup.model;

import java.util.Collection;
import java.util.List;

/**
 * A template provides an immutable object capable of creating elements
 * of a given type.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 * @param <E> type of element(s) created by the template
 */
public interface Template<E extends Element<?>>
extends Element<E>
{

	/**
	 * Get the name of the elements produced by this template.
	 * 
	 * @return element name
	 */
	@Override
	public String name();
	
	/**
	 * Get the children of the elements produced by this template.  The 
	 * resulting elements will be cloned by the template for each element
	 * created from the template.
	 * 
	 * @return child elements
	 */
	@Override
	public List<Element<?>> children();
	
	/**
	 * Get the attributes of the elements produced by this template.  The 
	 * resulting attributes will be cloned by the template for each element
	 * created from the template.
	 * 
	 * @return attributes
	 */
	@Override
	public Collection<Attribute> attributes();
	
	/**
	 * Create a new element from the template. 
	 * 
	 * @return element created from the template
	 */
	public E element();
	
}