package org.umarkup.model;

public class MuAttribute 
implements Attribute
{
	
	private String name;
	private String value;
	
	private String _str;

	public MuAttribute(String name)
	{
		this(name, null);
	}
	
	public MuAttribute(String name, String value)
	{
		this.name = name;
		this.value = value;
		this._str = null;
	}
	
	@Override
	public String 
	name()
	{
		return this.name;
	}

	@Override
	public String 
	value()
	{
		return this.value;
	}

	@Override
	public Attribute 
	name(String name)
	{
		this._str = null;
		this.name = name;
		return this;
	}

	@Override
	public Attribute 
	value(int value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(long value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(float value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(double value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(boolean value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(Object value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute 
	value(String value)
	{
		this._str = null;
		this.value = String.valueOf(value);
		return this;
	}

	@Override
	public Attribute
	value(String value, boolean escape)
	{
		this._str = null;
		this.value = escape ? escape(value) : value;
		return this;
	}

	@Override
	public Attribute 
	value(char separator, String... values)
	{
		this._str = null;
		
		StringBuilder sb = new StringBuilder(8 * values.length);
		
		int i = 0;
		for (String v : values)
		{
			sb.append(v);
			if (++i < values.length)
			{
				sb.append(separator);
			}
		}
		this.value = sb.toString();
		return this;
	}
	
	public String 
	toString()
	{
		if (this._str != null)
		{
			return this._str;
		}
		
		String value = this.value;
		if (value == null)
		{
			value = "";
		}
		
		StringBuilder sb = 
				new StringBuilder(this.name.length() + 3 + 
									value.length());
		
		this._str =  sb.append(this.name).append('=')
					.append('"').append(value).append('"').toString();
		
		return this._str;
	}

	private static final String 
	escape(String value)
	{
		return value.replace("&", "&amp;")
					.replace("<", "&lt;")
					.replace(">", "&gt;");
	}
}
