package org.umarkup.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Basic element implementation.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public class MuElement 
implements MutableElement<MuElement>, Cloneable
{
	
	private String name;
	private Map<String, Attribute> attributes;
	private List<Element<?>> children;
	
	private CloseType closing = CloseType.EXPLICIT;
	
	/**
	 * Construct a new element with the given name.
	 * 
	 * @param name element name
	 */
	public MuElement(String name)
	{
		this(name, new HashMap<String, Attribute>(0), 
				new LinkedList<Element<?>>());
	}
	
	private MuElement(String name, Map<String, Attribute> attributes, 
						List<Element<?>> children)
	{
		this.name = name;
		this.attributes = new HashMap<String, Attribute>(attributes);
		this.children = children;
	}
	
	@Override
	public MuElement 
	name(String name)
	{
		this.name = name;
		return this;
	}

	@Override
	public String 
	name()
	{
		return this.name;
	}

	@Override
	public Collection<Attribute> 
	attributes()
	{
		Collection<Attribute> c = 
				new ArrayList<Attribute>(this.attributes.values());
		return Collections.unmodifiableCollection(c);
	}
	
	@Override
	public Attribute
	attribute(String name)
	{
		Attribute attr = this.attributes.get(name);
		
		if (attr == null)
		{
			attr = new MuAttribute(name);
			this.attributes.put(name, attr);
		}
		return attr;
	}

	@Override
	public MuElement 
	attribute(Attribute attribute)
	{
		this.attributes.put(attribute.name(), attribute);
		return this;
	}
	
	@Override
	public MuElement
	attribute(String name, String value)
	{
		this.attributes.put(name, new MuAttribute(name, value));
		return this;
	}

	@Override
	public MuElement 
	attribute(String name, char delimiter, String... values)
	{
		this.attributes.put(name, 
							new MuAttribute(name).value(delimiter, values));
		return this;
	}

	@Override
	public MuElement
	attributes(Collection<Attribute> attributes)
	{
		for (Attribute attribute : attributes)
		{
			this.attributes.put(attribute.name(), attribute);
		}
		return this;
	}
	
	@Override
	public MuElement 
	attributes(Attribute... attributes)
	{
		for (Attribute attribute : attributes)
		{
			this.attributes.put(attribute.name(), attribute);
		}
		return this;
	}

	@Override
	public List<Element<?>> 
	children()
	{
		return Collections.unmodifiableList(this.children);
	}

	@Override
	public MuElement 
	child(Element<?> child)
	{
		this.children.add(child);
		return this;
	}

	@Override
	public MuElement 
	child(String child)
	{
		this.children.add(new StringElement(child));
		return this;
	}

	@Override
	public MuElement 
	children(List<Element<?>> children)
	{
		this.children.addAll(children);
		return this;
	}

	@Override
	public MuElement 
	children(Element<?> ... children)
	{
		for (Element<?> e : children)
		{
			this.children.add(e);
		}
		return this;
	}

	@Override
	public MuTemplate
	template()
	{
		return new MuTemplate(this);
	}
	
	@Override
	public CloseType 
	closing()
	{
		return this.closing;
	}

	@Override
	public MuElement
	close(CloseType policy)
	{
		this.closing = policy;
		return this;
	}
	
	@Override
	public Object
	clone() 
	{
		return new MuElement(this.name, 
							new HashMap<String, Attribute>(this.attributes), 
							new LinkedList<Element<?>>(this.children))
					.close(this.closing());
	}
	
	public static final MuElement
	from(String name)
	{
		return new MuElement(name);
	}
	

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributes == null) ? 0 : attributes.hashCode());
		result = prime * result
				+ ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((closing == null) ? 0 : closing.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MuElement other = (MuElement) obj;
		if (attributes == null)
		{
			if (other.attributes != null)
				return false;
		}
		else if (!attributes.equals(other.attributes))
			return false;
		if (children == null)
		{
			if (other.children != null)
				return false;
		}
		else if (!children.equals(other.children))
			return false;
		if (closing != other.closing)
			return false;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}



	protected static class MuTemplate
	implements Template<MuElement>
	{
		
		private final MuElement src;
		
		protected MuTemplate(MuElement src)
		{
			this.src = (MuElement) src.clone();
		}

		@Override
		public String 
		name()
		{
			return this.src.name;
		}

		@Override
		public List<Element<?>> 
		children()
		{
			return src.children();
		}

		@Override
		public Collection<Attribute> 
		attributes()
		{
			return this.src.attributes();
		}

		@Override
		public MuElement 
		element()
		{
			return (MuElement) this.src.clone();
		}

		@Override
		public Template<MuElement> 
		template()
		{
			return this;
		}

		@Override
		public CloseType 
		closing()
		{
			return this.src.closing();
		}
		
	}
}
