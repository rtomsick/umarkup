package org.umarkup.model;


/**
 * An attribute of an element.  Consists of a free-form name and a value 
 * string.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public interface Attribute
{
	
	/**
	 * Get the name of the attribute.
	 * 
	 * @return attribute name
	 */
	public String name();
	
	/**
	 * Get the string value of the attribute.
	 * 
	 * @return attribute value
	 */
	public String value();

	/**
	 * <p>
	 * Set the name of the attribute.
	 * </p>
	 * 
	 * <p>
	 * It is recommended that the given name not contain whitespace.
	 * </p>
	 * 
	 * @param name attribute name
	 * @return attribute with the given name set
	 */
	public Attribute name(String name);
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given integer value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(int value);
	
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given long value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(long value);
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given float value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(float value);
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given double value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(double value);
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given boolean value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(boolean value);
	
	/**
	 * Set the value of the attribute to the string representation of the 
	 * given object as returned by its implementation of 
	 * {@link Object#toString()}.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(Object value);
	
	/**
	 * Set the value of the attribute to the given string value.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(String value);
	
	/**
	 * Set the value of the attribute to the given string value.  The given
	 * string will have any entities contained within it escape prior to being
	 * set.
	 * 
	 * @param value attribute value
	 * @return attribute with the given value set
	 */
	public Attribute value(String value, boolean escape);
	
	/**
	 * Set the value of the attribute to the concatenation of the given 
	 * strings using the given character as a delimiter.
	 * 
	 * @param separator delimiter character
	 * @param values strings to concatenate as the value of the attribute
	 * @return attribute with the given value set
	 */
	public Attribute value(char separator, String ... values);

}
