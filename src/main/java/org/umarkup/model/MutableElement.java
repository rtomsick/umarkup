package org.umarkup.model;

import java.util.Collection;
import java.util.List;

/**
 * A mutable element is an element whose properties (name, children, and
 * attributes) may be changed.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 * @param <E> element type
 */
public interface MutableElement<E extends Element<?>>
extends Element<E>
{
	
	/**
	 * Set the name of the element.
	 * 
	 * @param name element name
	 * @return element with the given name set
	 */
	public E name(String name);

	/**
	 * Add the given element as a child of the element.
	 *  
	 * @param child child element
	 * @return element with the given child added
	 */
	public E child(Element<?> child);
	
	/**
	 * Add the given content as a child element of the element.
	 * 
	 * @param child child content
	 * @return element with the given child added
	 */
	public E child(String child);
	
	/**
	 * Add the given elements as children of the element in the order which 
	 * they appear in the given list.
	 * 
	 * @param children children to add
	 * @return element with the given children added
	 */
	public E children(List<Element<?>> children);
	
	/**
	 * Add the given elements as children of the element in the order which 
	 * they are provided
	 * 
	 * @param children children to add
	 * @return element with the given children added
	 */
	public E children(Element<?> ... children);
	
	/**
	 * Get the attribute of the element with the given name, creating one if 
	 * it does not exist.  The resulting attribute will be mutable; changes 
	 * to the returned reference will affect the element from which it was 
	 * obtained.
	 * 
	 * @param name name of attribute
	 * @return attribute with the given name
	 */
	public Attribute attribute(String name);
	
	/**
	 * Add the given attribute to the element.  The specified attribute will
	 * replace any attribute with the same name already present on the 
	 * element.
	 * 
	 * @param attribute attribute to add
	 * @return element with the given attribute added
	 */
	public E attribute(Attribute attribute);
	
	/**
	 * Add an attribute with the given name and value. The specified attribute 
	 * will replace any attribute with the same name already present on the 
	 * element.
	 * 
	 * @param name attribute name
	 * @param value attribute value
	 * @return element with the given attribute added
	 */
	public E attribute(String name, String value);
	
	/**
	 * <p>
	 * Add an attribute with the given name and value(s).  The given values
	 * will be included as the value of the specified attribute with the given
	 * character used as a delimiter.
	 * </p>
	 * 
	 * <p>
	 * This method is functionally identical to addition of an attribute which
	 * has had its value set via {@link Attribute#value(char, String...)}.
	 * </p>
	 * 
	 * @param name name of the attribute to create/set
	 * @param delimiter delimiter used to separate values
	 * @param values value(s) of the attribute
	 * @return element with the specified attribute set
	 */
	public E attribute(String name, char delimiter, String ... values);
	
	/**
	 * Add the given attributes to the element in the order returned by the 
	 * given collection's iterator.  The specified attributes will replace 
	 * any attributes with the same name already present on the element.
	 * 
	 * @param attributes attributes to add
	 * @return element with the given attributes added
	 */
	public E attributes(Collection<Attribute> attributes);
	
	/**
	 * Add the given attributes to the element in the order in which they are
	 * provided.  The specified attributes will replace any attributes with 
	 * the same name already present on the element.
	 * 
	 * @param attributes attributes to add
	 * @return element with the given attributes added
	 */
	public E attributes(Attribute ... attributes);
	
	/**
	 * Specify the element closing policy.
	 * 
	 * @param policy policy.
	 * @return element with the given closing policy set
	 */
	public E close(CloseType policy);
}
