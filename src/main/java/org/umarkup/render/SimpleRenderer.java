package org.umarkup.render;

import java.util.Collection;
import java.util.List;

import org.umarkup.model.AnonymousElement;
import org.umarkup.model.Attribute;
import org.umarkup.model.Element;
import org.umarkup.model.Element.CloseType;

/**
 * A simple renderer implementation.  Performs no pretty-printing or 
 * indentation.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public class SimpleRenderer 
implements Renderer
{

	private static final int _EST_CHILD_SIZE = 128;
	private static final int _EST_ATTRIB_SIZE = 32;

	public CharSequence 
	render(Element<?> element)
	{
		if (element instanceof AnonymousElement)
		{
			return ((AnonymousElement) element).content();
		}
		
		final List<Element<?>> children = element.children();
		final Collection<Attribute> attributes = element.attributes();
		
		final int estChildSize = _EST_CHILD_SIZE * children.size();
		final int estSize = 4 + (element.name().length() * 2) + 
							estChildSize + 
							(_EST_ATTRIB_SIZE * attributes.size()) + 16;
		
		
		
		StringBuilder sb = new StringBuilder(estSize);
		
		sb.append('<').append(element.name());
		
		for (Attribute attribute : attributes)
		{
			sb.append(' ').append(attribute.toString());
		}
		
		if (element.closing() == CloseType.SELF && children.isEmpty())
		{
			return sb.append("/>");
		}
		else
		{
			sb.append('>');
		}
		
		for (Element<?> e : children)
		{
			sb.append(this.render(e));
		}
		
		if (element.closing() == CloseType.NEVER ||
			((element.closing() == CloseType.CHILDREN) && 
					children.isEmpty()))
		{
			return sb;
		}
		
		return sb.append("</").append(element.name()).append('>');
	}
}
