package org.umarkup.render;

import java.util.List;

import org.umarkup.model.AnonymousElement;
import org.umarkup.model.Attribute;
import org.umarkup.model.Element;
import org.umarkup.model.Element.CloseType;

/**
 * <p>
 * Simple renderer which provides basic element indenting.  Children of a 
 * rendered element will be indented with <i>n</i> instances of the 
 * specified indentation string (see {@link #indentString(String)}) where 
 * <i>n</i> is the depth within the rendered element's children hierarchy.
 * </p>
 * 
 * <p>
 * In the case of elements which contain only a single child of the type
 * {@link AnonymousElement}, the child element will be rendered with no 
 * indentation and no line breaks before/after (i.e. inline with the parent's
 * opening and closing output.)
 * </p>
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public class IndentingRenderer
implements Renderer
{
	
	/**
	 * Default indentation string.
	 */
	public static final String DEFAULT_INDENT_STRING = "\t";
	
	/**
	 * Default line break sequence.
	 */
	public static final String DEFAULT_LINE_BREAK = "\n";

	private String indentString = DEFAULT_INDENT_STRING;
	
	private String lineBreak = DEFAULT_LINE_BREAK;
	
	@Override
	public CharSequence 
	render(Element<?> element)
	{
		return this.render(element, 0);
	}
	
	/**
	 * Set the indentation string used to indent rendered elements.
	 * 
	 * @param indentString indentation string
	 * @return renderer instance
	 */
	public IndentingRenderer
	indentString(String indentString)
	{
		this.indentString = indentString;
		return this;
	}
	
	/**
	 * Set the line break sequence used to indicate new lines within rendered
	 * output.
	 * 
	 * @param lineBreakSequence line break sequence
	 * @return renderer instance
	 */
	public IndentingRenderer
	lineBreak(CharSequence lineBreakSequence)
	{
		this.lineBreak = lineBreakSequence.toString();
		return this;
	}
	
	private CharSequence
	render(Element<?> element, int depth)
	{
		StringBuilder builder = new StringBuilder(128);
		
		List<Element<?>> children = element.children();
		
		for (int i = 0; i < depth; i++)
		{
			builder.append(this.indentString);
		}
		
		/* If we have an anonymous element, just return the element (since 
		 * closing and child handling are unnecessary.)
		 */
		if (element instanceof AnonymousElement)
		{
			return builder.append(((AnonymousElement) element).content());
		}
		
		builder.append('<').append(element.name());
		
		for (Attribute attribute : element.attributes())
		{
			builder.append(' ').append(attribute.toString());
		}
		
		if (element.closing() == CloseType.SELF && children.isEmpty())
		{
			return builder.append("/>");
		}
		else
		{
			builder.append('>');
		}
		
		
		for (Element<?> child : children)
		{
			/* If we have an anonymous element with no children then we 
			 * just output its contents and close.  This means we don't indent
			 * or prepend a line break to the closing element/
			 */
			if (child instanceof AnonymousElement && children.size() == 1)
			{
				builder.append(((AnonymousElement) child).content());
				close(element, builder, depth, "");
				return builder;
			}
			
			builder.append(this.lineBreak);
			
			builder.append(this.render(child, depth + 1));
		}
		

		builder.append(this.lineBreak);
		
		
		close(element, builder, depth, this.indentString);
		
		
		return builder;
	}
	
	/**
	 * Append closing output for the given element to the given builder (if
	 * applicable.)  Output will be indented to the specified depth using
	 * the given indentation string.
	 * 
	 * @param element element for which to render closing output
	 * @param builder builder to which to output element closing
	 * @param depth element depth
	 * @param indentString indentation string
	 */
	private static void
	close(Element<?> element, StringBuilder builder, int depth, 
			String indentString)
	{
		if (element.closing() == CloseType.NEVER ||
			((element.closing() == CloseType.CHILDREN) && 
					element.children().isEmpty()))
		{
			return;
		}
		
		for (int i = 0; i < depth; i++)
		{
			builder.append(indentString);
		}
		
		builder.append("</").append(element.name()).append('>');
	}
}
