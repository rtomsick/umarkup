package org.umarkup.render;

import org.umarkup.model.Element;

/**
 * A renderer can render elements to character sequences.  Implementations of
 * this interface may add pretty-printing of whitespace, etc.  but are under 
 * no obligation to do anything more than output the tag(s) and attribute(s) 
 * that correspond to a given element.
 * 
 * @author Robert Tomsick (robert@tomsick.net)
 *
 */
public interface Renderer
{

	/**
	 * Render the given element to a character sequence.
	 * 
	 * @param element element to render
	 * @return character sequence containing a rendered representation of the
	 * given element and its children
	 */
	public CharSequence render(Element<?> element);
	
}
