# μMarkup

## What is it?
μMarkup is my attempt at a Java library to ease the pain of programmatic markup generation. It is lightweight, reasonably-fast, very simple to use, and covers the boring, common, and tedious "90% tasks".

I wrote it because all of the existing Java APIs for this are some combination of: 1) painfully verbose 2) deprecated/unsupported 3) designed for XML and producing HTML with them requires plenty of bludgeoning and nasty hacks 4) templating systems that suck when used outside of the wonderful world of web apps.

I just wanted to produce some HTML, damnit.

## What is μMarkup not?

* It is not a proper DOM implementation. It has no concept of Nodes or Documents. It knows about Elements and Attributes and that's about it. These Elements and Attributes don't have anything to do with XML.

* It is not an XML library. If you need namespace support, or a parser, or pretty much anything to do with XML, μMarkup is probably not the best choice.

* It is not a templating language/library. There are a lot of those. Some of them are awesome. Most of them are not. None of them are terribly good for programmatic markup creation; μMarkup is.

* It is not limited to HTML. It has a couple of features to make it more convenient to create HTML, but it can just as easily be used to produce SGML or XHTML (although in the case of the latter you really should be using something like the excellent JDOM library.)

## So is it stable?

μMarkup has been in production usage (generating the markup for something in the high hundreds of thousands of requests per day) for several years now.

In that time, no bugs have been found.

That does not, however, mean that there are no bugs!  Just because my [employer's] use case doesn't expose bugs doesn't mean yours won't.  If you find them, feel free to report them.  Bug reports are always welcome.

## Why should I use it?

You want to generate HTML or other markup programmatically. Perhaps you have some data that you need to render and the templates that you've tried using to do so end up in a twisted mass of spaghetti code and crazy nested tags... and you think "you know, it would be so much easier if I could just build this stuff on the fly." And then you try it, and after about fifteen minutes you're ready to drink yourself to death if you see another .append()

This was my motivation for writing μMarkup.

It doesn't make it impossible to generate weird markup. It just makes it easier to build what you want.

* You've looked at other solutions, but they're either deprecated and/or limited to HTML4 (ala. ECS) or aren't very convenient to use outside of a web context (JSP et al.) μMarkup is designed to be easy to use and to offer a convenient, simple API.  IMHO, an API that is less-featureful but enjoyable to use trumps one that's full-featured but unpleasant.

* μMarkup is flexible. This is the corollary to the "doesn't stop you from creating invalid markup" property above: you're free to create markup that looks however you'd like. Don't want a namespace declaration or a character encoding meta tag or whatever? No sweat. You don't have to add anything you don't want to (even if you really *should* produce valid HTML... seriously, don't be that guy.)

* μMarkup is BSD licensed. 2-clause BSD license, specifically. Use it in whatever products/projects you'd like, just reproduce the necessary bits of the license and you're golden.

* It's easy to wrap. You can certainly create elements via object instantiation, setting properties, etc. -- the fluent interface helps with this -- but μMarkup is also designed to make it easy to wrap your common uses into whatever library or abstraction you'd like to build (see Template objects within the org.umarkup.model package.

## License

μMarkup is under the two-clause BSD license.  See `LICENSE` for a copy of it.